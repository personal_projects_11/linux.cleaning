# Manjaro Clean up

```
# Vacuum journals
sudo journalctl --vacuum-size=500M && sudo journalctl --vacuum-time=7d

# Remove all uninstalled packages
sudo paccache -rvuk0

# Remove old installed packages, leave 3
sudo paccache -rvk3

# Clean yay cache
yay -Sc -a

# Clean pamac build cache
pamac clean -bv

# Clean temporary build files
rm -rf ~/{.bundle,.cargo,.cmake,.dotnet,.electron,.electron-gyp,.gem,.gradle,.lazarus,.node-gyp,.npm,.nuget,.nvm,.racket,.rustup,.stack,.yarn} || true
rm -rf ~/.cache/{electron,electron-builder,go-build,node-gyp,pip,yarn} || true
sudo rm -rf ~/go || true

# Find files not owned by any package
sudo lostfiles
```

## Delete cache

```
find ~/.cache -depth -type f -mtime +100 -delete
```

## See program's cache sizes

```
du -shc $HOME/.cache/* | sort -hr | head -10
```

### Delete chrome cache

In the profile via UI, is the best chance

## Docker clean caches

See sizes

```
docker system df
```

Clean all
```
docker system prune -a
```

**Be aware of this**
	
	WARNING! This will remove:
	  - all stopped containers
	  - all networks not used by at least one container
	  - all images without at least one container associated to them
	  - all build cache

## References

* Manjaro
	* https://forum.manjaro.org/t/cleaning-up-and-freeing-disk-space/6703/31?page=2
	* https://forum.manjaro.org/t/how-to-wipe-temp-files-to-get-more-free-space-manjaro-kde/64529/5
* Chrome
	* https://askubuntu.com/questions/628460/how-to-delete-temp-files-which-is-created-by-google-chrome-in-linux
	* https://linuxhint.com/removing_cache_chrome/
* Docker
	* https://docs.docker.com/engine/reference/commandline/system_prune/
	* https://gist.github.com/bastman/5b57ddb3c11942094f8d0a97d461b430
	* https://www.freecodecamp.org/news/how-to-remove-all-docker-images-a-docker-cleanup-guide/